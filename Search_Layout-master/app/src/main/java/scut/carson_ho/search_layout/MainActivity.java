package scut.carson_ho.search_layout;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

public class MainActivity extends BaseActivity {

    private Button button;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSetting();
    }
    /**
     * 顶部标题栏按键设置
     */
    public void setSetting(){
        //右边的设置键是否显示
        setOkVisibity(true);
        //设置右边按钮显示的内容，不设置为'设置'选项，默认为'设置'
        setOKText("设置");
        //左边的返回键是否显示,如果不需要显示，请设置text为空，不要直接设置为false
        setBackVisibity(true);
        setLeftText("搜索");
        //设置标题文字，默认显示'标题'
        setTitleText("首页");
    }
    //重写返回健点击事件，设置为打开搜索，如果不重写，则采用返回的点击事件
    public void back(View view){
        startActivity(new Intent(MainActivity.this, SearchDemo.class));
    }

    /**
     * 重写首页的返回键点击两次后退出，并且提示
     * 说明：通过两次点击返回按钮时的时间间隔进行判断。
     * 实现方式：主要是用第二次点击返回按钮时的时间减去第一次点击的返回按钮时的时间
     *           看这个时间差是否大于2秒
     *           如果大于2秒则提示需要再点击一次才能退出
     *           如果时间差小于等于2秒则退出应用
     */
    private long exitTime = 0;
    @Override
    public void onBackPressed() {
        if(System.currentTimeMillis() - exitTime > 2000) {
            Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
            exitTime = System.currentTimeMillis();
        } else {
            finish();
            System.exit(0);
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }
}

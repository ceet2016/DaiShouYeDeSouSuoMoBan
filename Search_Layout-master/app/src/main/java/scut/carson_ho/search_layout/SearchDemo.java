package scut.carson_ho.search_layout;

import android.os.Bundle;

import scut.carson_ho.searchview.ICallBack;
import scut.carson_ho.searchview.SearchView;
import scut.carson_ho.searchview.bCallBack;

/**
 * Created by Carson_Ho on 17/8/11.
 */

public class SearchDemo extends BaseActivity {

    // 1. 初始化搜索框变量
    private SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setSetting();
        // 2. 绑定视图
        setContentView(R.layout.activity_search);

        // 3. 绑定组件
        searchView = (SearchView) findViewById(R.id.search_view);

        // 4. 设置点击搜索按键后的操作（通过回调接口）
        // 参数 = 搜索框输入的内容
        searchView.setOnClickSearch(new ICallBack() {
            @Override
            public void SearchAciton(String string) {
                System.out.println("我收到了" + string);
            }
        });

        // 5. 设置点击返回按键后的操作（通过回调接口）
        searchView.setOnClickBack(new bCallBack() {
            @Override
            public void BackAciton() {
                finish();
            }
        });
    }
    /**
     * 顶部标题栏按键设置
     */
    public void setSetting(){
        //右边的设置键是否显示
        setOkVisibity(true);
        //设置右边按钮显示的内容，不设置为'设置'选项，默认为'设置'
        setOKText("设置");
        //左边的返回键是否显示,如果不需要显示，请设置text为空，不要直接设置为false
        setBackVisibity(true);
        setLeftText("返回");
        //设置标题文字，默认显示'标题'
        setTitleText("搜索");
    }
}